//
//  DetailView.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import SwiftUI

struct DetailView: View {
    @StateObject var viewModel = ViewModel()
    @State var onClick: Bool = false
    
    var teamName: String
    var descTeam: String
    var img: String
    
    let userDefault = UserDefaults.standard
//    @State var favorite: [String]
//    @AppStorage var favorite = UserDefaults.standard.array(forKey: "teamName")
    @AppStorage("teamName") var favorite: [String] = []
    
//    mutating func addFavorite(id: String, name: String, descTeam: String)-> [Favorite] {
//        self.favorite.append(Favorite(id: id , name: name, desc: descTeam))
//        return Favorite
//    }
    var body: some View {
        ScrollView {
            VStack {
                HStack {
                    Text(teamName)
                        .font(.title)
                        .foregroundColor(.primary)
                    
                    Spacer()
                    
                    Button(action: {self.onClick.toggle()
                        if self.onClick == true {
//                            userDefault.set(["kencan", "turu"], forKey: "teamName")
                            print(UserDefaults.standard.array(forKey: "teamName"))
                            print(favorite)
                            favorite.append(teamName)
//                            print(favorite)

                            userDefault.set(favorite, forKey: "teamName")
                            print(UserDefaults.standard.array(forKey: "teamName"))
                        } else {
                            userDefault.removeObject(forKey: "teamName")
                            print("keluar")
                        }
                        }) {
                            Image(systemName: self.onClick == true ? "star.fill" : "star")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(.yellow)
                    }
                }
                .padding()
                
                AsyncImage(url: URL(string: img), scale: 2)
                
                Text(descTeam)
                    .font(.headline)
                    .padding()
            }
        }
        .navigationTitle(teamName)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(teamName: "", descTeam: "", img: "", favorite: [])
    }
}
