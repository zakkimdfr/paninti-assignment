//
//  ContentView.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import SwiftUI

struct LeagueView: View {
    @StateObject var viewModel = ViewModel()
    
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.leagues, id: \.idLeague) {league in
                    NavigationLink(destination: TeamView(leagueName: league.strLeague)) {
                        LazyVStack {
                            Text(league.strLeague)
                                .font(.headline)
                                .foregroundColor(.primary)
                            
                            Text(league.strSport)
                                .foregroundColor(.secondary)
                        }
                    }
                }
            }
            .navigationTitle("Leagues")
            .onAppear{
                viewModel.fetch()
            }
        }
    }
}





struct LeagueView_Previews: PreviewProvider {
    static var previews: some View {
        LeagueView()
    }
}
