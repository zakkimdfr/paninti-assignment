//
//  TeamView.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import SwiftUI

struct TeamView: View {
    @StateObject var viewModel = ViewModel()
    var leagueName: String
    
    var body: some View {
        List {
            ForEach(viewModel.teams, id: \.?.idTeam) {team in
                NavigationLink(destination: DetailView(teamName: team?.strTeam ?? "", descTeam: team?.strDescriptionEN ?? "", img: team?.strTeamLogo ?? "", favorite: [])) {
                    LazyVStack {
                        Text(team?.strTeam ?? "")
                            .font(.headline)
                    }
                }
            }
        }
        .navigationTitle(leagueName)
        .onAppear{
            viewModel.fetchTeamByLeague(league: leagueName)
        }
    }
}

struct TeamView_Previews: PreviewProvider {
    static var previews: some View {
        TeamView(leagueName: "")
    }
}
