//
//  TabBarView.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import SwiftUI

struct TabBarView: View {
    var body: some View {
        TabView {
            LeagueView()
                .tabItem {
                    Image(systemName: "play.rectangle")
                    Text("League")
                }
            FavoriteView(favorite: [Favorite(id: "", name: "", desc: "")])
                .tabItem {
                    Image(systemName: "star.square")
                    Text("Favorites")
                }
        }
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
