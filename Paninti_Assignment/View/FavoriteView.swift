//
//  FavoriteView.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import SwiftUI

struct FavoriteView: View {
    let userDefault = UserDefaults.standard
    var favorite: [Favorite]
    
    var body: some View {
//        print(userDefault.object(forKey: "teamName"))
//        return Text("print")
        List {
            ForEach(((userDefault.object(forKey: "teamName") as? [String] ?? [String]())), id: \.self) {team in
                Text(team)
            }
        }
    }
}

struct FavoriteView_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteView(favorite: [Favorite(id: "", name: "", desc: "")])
    }
}
