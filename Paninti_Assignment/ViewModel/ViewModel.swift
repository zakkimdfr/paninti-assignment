//
//  ViewModel.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import Foundation
import SwiftUI

class ViewModel: ObservableObject {
    @Published var leagues: [League] = []
    @Published var teams: [Team?] = []
    
    let baseURL = "https://www.thesportsdb.com/api/v1/json"
    
    func fetch() {
        guard let url = URL(string: "\(baseURL)/3/all_leagues.php") else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, _ , error in
            guard let data = data, error == nil else {
                return
            }
            
            do {
                let leagues = try JSONDecoder().decode(LeagueResponse.self, from: data)
                DispatchQueue.main.async {
                    self?.leagues = leagues.leagues
                }
            }catch {
                print(error)
            }
        }
        task.resume()
    }
    
    func fetchTeamByLeague(league: String) {
        let replaceString = league.replacingOccurrences(of: " ", with: "%20")
        
        guard let url = URL(string: "\(baseURL)/3/search_all_teams.php?l=\(replaceString)") else {
            return
        }
        
        print("url", url)
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, _ , error in
            guard let data = data, error == nil else {
                return
            }
            
            do {
//                let hasil = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
//                print(hasil)
                
                let teams = try JSONDecoder().decode(TeamResponse.self, from: data)
                DispatchQueue.main.async {
                    self?.teams = teams.teams
                }
            }catch {
                print(error)
            }
        }
        task.resume()
    }
}
