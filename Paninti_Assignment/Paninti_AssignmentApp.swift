//
//  Paninti_AssignmentApp.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import SwiftUI

@main
struct Paninti_AssignmentApp: App {
    var body: some Scene {
        WindowGroup {
            TabBarView()
        }
    }
}
