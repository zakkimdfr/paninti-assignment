//
//  Team.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import Foundation

struct Team: Codable {
    var idTeam: String
    var idSoccerXML: String
    var strTeam: String
    var strDescriptionEN: String
    var strTeamLogo: String
//    var strAlternate: String?
//    var intFormedYear: Int?
//    var strSport: String?
//    var strLeague: String?
//    var strLeague2: String?
//    var strLeague3: String?
//    var strStadium: String?
//    var strStadiumThumb: String?
//    var strStadiumDescription: String?
//    var strTeamLogo: String?
}

struct TeamResponse: Codable {
    var teams: [Team]
}
