//
//  League.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 07/03/23.
//

import Foundation

struct League: Codable {
    var idLeague: String
    var strLeague: String
    var strSport: String
    var strLeagueAlternate: String?
}

struct LeagueResponse: Codable {
    var leagues: [League]
}
