//
//  Favorite.swift
//  Paninti_Assignment
//
//  Created by Zakki Mudhoffar on 09/03/23.
//

import Foundation

struct Favorite {
    var id: String
    var name: String
    var desc: String
}
